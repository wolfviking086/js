// ===================== З а д а н и е 1  ======================================== 

let password = "pass-";
let x = "-";
let y = "_";

if (password.length >= 4 && password.includes(x) || password.includes(y)) {
    console.log('Пароль надёжный');
} else {
    console.log('Пароль недостаточно надёжный');
}


// ===================== З а д а н и е 2  ========================================                

const name = "nick";
const name2 = name.toLowerCase();
const surname = "Pavlov";
const surname2 = surname.toLowerCase();

// делим слово на отдельные буквы
const splitted = name.split("");
const splitted2 = surname.split("");

// делаем первую букву в массиве заглавной 
const first = splitted[0].toUpperCase() + name2.substring(1);
const second = splitted2[0].toUpperCase() + surname2.substring(1);

if (name === first && surname === second) {
    console.log("Имя осталось без изменений");
} else {
    console.log("Имя было преобразовано");
}